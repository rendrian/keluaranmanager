<?php

use Illuminate\Database\Seeder;
use App\Model\DataProvider;
use App\User;
use Illuminate\Support\Facades\Hash;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $x = array(
            ["Singapore", "SGP", "https://1.bp.blogspot.com/-lxlQ2hfuSqk/XnyrrmUwwvI/AAAAAAAAJtM/5XkjzFEPVg0zkOl9_RAOPr0TywtZmRNJACLcBGAsYHQ/s1600/Singapore.jpg", null, null, "1"],
            ["Sydney", "SD", "https://1.bp.blogspot.com/-W9-Q-NUci_Q/Xnyrty_hEnI/AAAAAAAAJtk/MnvVfhyZ6z8w2JKKdeUN2sCRt_i_TR6NQCLcBGAsYHQ/s1600/sydney.jpg", null, null, "2"],
            ["Hongkong", "HK", "https://1.bp.blogspot.com/-KdeykL7SYbo/Xnyq-2bCCmI/AAAAAAAAJsQ/iJeRWjXUQzUzJcl0aD0NrYarLSQNx6gOwCLcBGAsYHQ/s1600/Hongkong.jpg", null, null, "3"],
            ["Timor", "TMR", "https://1.bp.blogspot.com/-Uea5FB1nOrk/XnyrtSezryI/AAAAAAAAJtc/C9Yfl_i-De4cW-SF_eIRFUUD4uCP8EhWQCLcBGAsYHQ/s1600/Timor.jpg", null, null, "4"],
            ["Bullseye", "BUL", "https://1.bp.blogspot.com/-858d1umKzWA/Xnyq96TRXoI/AAAAAAAAJsI/bMq43CfP8L8LW5djZD1B5eiA4KjjdmcCgCLcBGAsYHQ/s1600/Bullseye.jpg", null, null, "5"],
            ["Beijing", "BEI", "https://1.bp.blogspot.com/-V6iVfekNe2o/Xnyq8O0QqtI/AAAAAAAAJr8/GWwfKzUWW_YLgEmfIy4ka2VSWs4Oj74qwCLcBGAsYHQ/s1600/Beijing.jpg", null, null, "6"],
            ["Tokyo", "TKY", "https://1.bp.blogspot.com/-nt75ldldcxw/Xnyrt7K2gEI/AAAAAAAAJtg/IDuQEDT3HsY_Rps_nTqq_myypIjL_pReQCLcBGAsYHQ/s1600/Tokyo.jpg", null, null, "7"],
            ["Seoul", "SOL", "https://1.bp.blogspot.com/-_a6mEMtEQEQ/XnyrsOWWofI/AAAAAAAAJtQ/ns47Nd2Ts4okm_LTaN1eUyArUIY4fuI3wCLcBGAsYHQ/s1600/Seoul.jpg", null, null, "8"],
            ["China", "CHN", "https://1.bp.blogspot.com/-kJXR1tjrfPQ/Xnyq-PktHWI/AAAAAAAAJsM/ouKlQ8gRvOc8OHm5ws7yfd1z3LFmEJ5uwCLcBGAsYHQ/s1600/China.jpg", null, null, "9"],
            ["Bangkok", "BK", "https://1.bp.blogspot.com/-7-PL7n9lb6Q/Xnyq7hFQFJI/AAAAAAAAJr4/oy3s66gB0dEJNiFRTDl1Yk8PpTaH_xsZwCLcBGAsYHQ/s1600/Bangkok.jpg", null, null, "10"],
            ["Singapore45", "SGP45", "https://1.bp.blogspot.com/-esG2iZOLXBw/XnyrsYRURWI/AAAAAAAAJtU/UETbAoqecEAOq4zobMksEF1U7fX9RD9UACLcBGAsYHQ/s1600/Singapore45.jpg", null, null, "11"],
            ["Milan", "MIL", "https://1.bp.blogspot.com/-ckAmGFPvkMw/XnyrpsBOOzI/AAAAAAAAJtA/p_bPdfKzxVQyVEPpXVfY4x65ZMQVLN5nACLcBGAsYHQ/s1600/Milan.jpg", null, null, "12"],
            ["Magnum4D", "MGN", "https://1.bp.blogspot.com/-RtX2wXCBzYM/XnyrAF5W8cI/AAAAAAAAJsc/QlzJ_osDkXg8sP6bDhcKC9z4zqPAZnQaACLcBGAsYHQ/s1600/Magnum4d.jpg", null, null, "13"],
            ["Malaysia", "MY", "https://1.bp.blogspot.com/-AxhHs3ItMds/XnyrAtXc78I/AAAAAAAAJsg/kaBmoXvwtWAt2ayFvoYXCGBBN3cX7bZMACLcBGAsYHQ/s1600/Malaysia.jpg", null, null, "14"],
            ["Japan", "JP", "https://1.bp.blogspot.com/-auFePNaRzMQ/Xnyq_vGpvnI/AAAAAAAAJsU/WjPFy8Kv_pQgx7ndKMyJ0l-Yok3gLWP2ACLcBGAsYHQ/s1600/Japan.jpg", null, null, "15"],
            ["Manila", "MNL", "https://1.bp.blogspot.com/-RoYp6rIAT9Q/Xnyropdx-2I/AAAAAAAAJs8/RKjUUloZi4U0Wy2jbeRQtP9yWYZ3juo8QCLcBGAsYHQ/s1600/Manila.jpg", null, null, "16"],
            ["Macau", "MC", "https://1.bp.blogspot.com/-42lgcwkQbTo/Xnyq_5j1JrI/AAAAAAAAJsY/uCnn2P_GC6AQ43Tie7shd2i_2H_yV5QQACLcBGAsYHQ/s1600/Macau.jpg", null, null, "17"],
            ["PCSO", "PCSO", "https://1.bp.blogspot.com/-gmIo0Tk5klI/XnyrpruvzRI/AAAAAAAAJtE/FO0zR0uIFCAc8HrB0tYtMZfiCZlP1mzgQCLcBGAsYHQ/s1600/PCSO.jpg", null, null, "18"],
            ["Taiwan", "TW", "https://1.bp.blogspot.com/-9PD3wh83S2Q/Xnyrs98Tl_I/AAAAAAAAJtY/GelxX_m2KgE8ZMQ8cZwpEwQgWzc1_4UkACLcBGAsYHQ/s1600/Taiwan.jpg", null, null, "19"],
            ["Qatar", "QTR", "https://1.bp.blogspot.com/-R84y6tO4cUk/XnyrqRDoXgI/AAAAAAAAJtI/Sbq66NJUPKwJVqTLHuuDvH3ZFQBXJmwfACLcBGAsYHQ/s1600/Qatar.jpg", null, null, "20"],
            ["Barcelona", "BCL", "https://1.bp.blogspot.com/-EbBamYy4KHs/Xnyq8Xug2SI/AAAAAAAAJsA/EYOSTtE-Sp4LAY83n98tWSRMwQib44cugCLcBGAsYHQ/s1600/Barcelona.jpg", null, null, "21"],
            ["Berlin", "BER", "https://1.bp.blogspot.com/-K7Tabujfj5k/Xnyq9XOOpnI/AAAAAAAAJsE/5ekPcH8CtJogI_JgYio3GYWxJ7ximn1OwCLcBGAsYHQ/s1600/Berlin.jpg", null, null, "22"],
        );
        foreach ($x as $data) {
            $new = new DataProvider;
            $new->nama = $data[0];
            $new->kode = $data[1];
            $new->gambar = $data[2];
            $new->keterangan = $data[3];
            $new->source = $data[4];
            $new->position = (int) $data[5];
            $new->save();
        }

        $new = new App\User;
        $new->name = "BGIBOLA";
        $new->email = "betgratisinfo@gmail.com";
        $new->password = Hash::make("aa12345678");
        $new->level = 'ADMIN';
        $new->save();
    }
}
