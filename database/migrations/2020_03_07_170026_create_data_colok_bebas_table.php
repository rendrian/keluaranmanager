<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataColokBebasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_colokbebas', function (Blueprint $table) {
            $table->id();
            $table->integer('id_keluaran');
            $table->integer('angka_0')->default(0);
            $table->integer('angka_1')->default(0);
            $table->integer('angka_2')->default(0);
            $table->integer('angka_3')->default(0);
            $table->integer('angka_4')->default(0);
            $table->integer('angka_5')->default(0);
            $table->integer('angka_6')->default(0);
            $table->integer('angka_7')->default(0);
            $table->integer('angka_8')->default(0);
            $table->integer('angka_9')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_colokbebas');
    }
}
