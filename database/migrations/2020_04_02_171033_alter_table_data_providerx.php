<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableDataProviderx extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_provider', function (Blueprint $table) {

            $table->string('hari_buka')->nullable()->default(json_encode(array("1", "2", "3", "4", "5", "6", "7")));
            $table->string('jam_buka')->nullable();
            $table->string('jam_tutup')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_provider', function (Blueprint $table) {
            $table->dropColumn(['hari_buka', 'jam_buka', 'jam_tutup']);
        });
    }
}
