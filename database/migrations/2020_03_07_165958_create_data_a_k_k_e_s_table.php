<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataAKKESTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_akke', function (Blueprint $table) {
            $table->id();
            $table->integer('id_keluaran');
            $table->integer('as');
            $table->integer('kop');
            $table->integer('kepala');
            $table->integer('ekor');
            $table->timestamps();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_akke');
    }
}
