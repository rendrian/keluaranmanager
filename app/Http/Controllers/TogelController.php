<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Helperx\AngkaDetect;
use App\Model\DataAKKE;
use App\Model\DataColokBebas;
use App\Model\DataKeluaran;
use App\Model\DataProvider;
use App\Model\DataShio;
use App\Model\DataUmum;
use Dotenv\Result\Success;
use Illuminate\Support\Facades\DB;
use Helperx\Facades\AngkaDetect;
use Symfony\Component\VarDumper\Cloner\Data;

class TogelController extends Controller
{

    //  api usage
    // keluaranby provider
    public function getLast30DayData($providerId, $limit = 30)
    {


        $providerInfo =  DataProvider::where('id', $providerId)->first();
        $data = DataKeluaran::select(
            'id',
            'hari',
            'tanggal',
            'periode',
            'keluaran',
            'provider'
        )->where('provider', $providerId)
            ->orderBy('tanggal', 'DESC')
            ->limit($limit)
            ->get();
        $response = [
            'providerInfo' => $providerInfo,
            'keluaran' => $data
        ];
        return response($response, 200);
    }
    public function keluaranDetails(Request $request)
    {
        $details = DataKeluaran::where('id', $request->id)->first();
        if ($details !== null) {
            return response([
                'success' => true,
                'msg' => "Data dengan $request->id tersedia",
                'details' => $details
            ], 200);
        } else {
            return response([
                'success' => false,
                'msg' => "Data dengan ID:  $request->id Tidak Ada",
                'details' => null
            ], 200);
        }
    }
    public function LastPeriode($id)
    {
        $last =  DataKeluaran::where('provider', $id)
            ->orderBy('tanggal', 'desc')
            ->first();
        if ($last === null) {
            return false;
        } else {
            return $last->periode + 1;
        }
    }

    public function base4($angka)
    {
        return str_pad($angka, 4, '0', STR_PAD_LEFT);
    }

    public function UpdateKeluaran(Request $request)
    {
        try {
            $data =   DataKeluaran::updateOrCreate([
                'id' => $request->id,
                'provider' => $request->provider
            ], [
                'periode' => $request->periode,
                'tanggal' => $request->tanggal,
                'hari' => $request->hari,
                'keluaran' => $this->base4($request->keluaran),

            ]);
            return response([
                'success' => true,
                'msg' => "Data berhasil di Update",
                'details' => $data,

            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => false,
                // 'msg' => 'Pastikan Tanggal Yang Anda Masukkan Belum Pernah Diinput Sebelumnya',
                'msg' => $th->getMessage(),

            ], 200);
        }
        // try {
        //     $data =    DataKeluaran::updateOrCreate([
        //         'id' => $request->id,

        //         'provider' => $request->provider
        //     ], [
        //         'periode' => $request->periode,
        //         'tanggal' => $request->tanggal,
        //         'hari' => $request->hari,
        //         'keluaran' => $this->base4($request->keluaran),

        //     ]);
        //     $hasilProses =  $this->_prosesKeluaran($request);
        // } catch (\Throwable $th) {
        // }
    }

    public function _prosesKeluaran($request, $type = 'update')
    {

        try {
            if ($type == 'update') {
                $iddata = $request->id;
            } else {

                $iddata = $request->id;
            }

            $hari = $request->hari;
            $tanggal = $request->tanggal;
            $periode = null;
            $keluaran = $request->keluaran;
            $keluaran = str_pad($keluaran, 4, '0', STR_PAD_LEFT);
            $last2 = substr($keluaran, 2);
            $hasilHitung =  AngkaDetect::hitungAngka($keluaran);


            $lastInsert = $request->id;


            // return $hasilHitung['_report'];
            // return $hasilHitung['_report'];
            $explodeColok =  explode(',', $hasilHitung['_report']['_colokBebas']);
            $arrayColok = [
                "id_keluaran" => $lastInsert,
                "angka_0" => $explodeColok[0],
                "angka_1" => $explodeColok[1],
                "angka_2" => $explodeColok[2],
                "angka_3" => $explodeColok[3],
                "angka_4" => $explodeColok[4],
                "angka_5" => $explodeColok[5],
                "angka_6" => $explodeColok[6],
                "angka_7" => $explodeColok[7],
                "angka_8" => $explodeColok[8],
                "angka_9" => $explodeColok[9],


            ];

            $arrayAkke = [
                'id_keluaran' => $lastInsert,
                'as' => $hasilHitung['_report']['_akke']['AS'],
                'kop' => $hasilHitung['_report']['_akke']['KOP'],
                'kepala' => $hasilHitung['_report']['_akke']['KEPALA'],
                'ekor' => $hasilHitung['_report']['_akke']['EKOR'],

            ];
            // insert shio
            $arrayShio = [
                "id_keluaran" => $lastInsert,
                "angka" => $last2,
                "shio" => $hasilHitung['_report']['_shio']
            ];

            DataShio::where('id_keluaran', $iddata)->delete();
            DataAKKE::where('id_keluaran', $iddata)->delete();
            DataUmum::where('id_keluaran', $iddata)->delete();
            DataColokBebas::where('id_keluaran', $iddata)->delete();


            DataShio::create($arrayShio);
            DataAKKE::create($arrayAkke);
            DataColokBebas::create($arrayColok);



            $array5050 = [];
            foreach ($hasilHitung['_report']['_5050']['umum_'] as $umum) {
                array_push($array5050, [
                    "id_keluaran" => $lastInsert,
                    "angka" => $last2,
                    "status" => $umum,
                    "jenis" => "UMUM"
                ]);
            }

            foreach ($hasilHitung['_report']['_5050']['kombinasi_'] as $kombinasi) {
                array_push($array5050, [
                    "id_keluaran" => $lastInsert,
                    "angka" => $last2,
                    "status" => $kombinasi,
                    "jenis" => "KOMBINASI"
                ]);
            }

            // hapus data umum lama

            foreach ($array5050 as $a5050) {
                DataUmum::create($a5050);
            }

            return [
                'success' => true,
                'msg' => "Data di update"
            ];
        } catch (\Throwable $th) {

            return [
                'success' => false,
                'msg' => $th->getMessage()
            ];
        }
    }
    public function DeleteKeluaran(Request $request)
    {
        try {
            $data = DataKeluaran::find($request->id)->delete();
            return response([
                'success' => true,
                'msg' => 'Data berhasil dihapus',
                'info' => $data
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'success' => true,
                'msg' => 'Data gagal dihapus',
                'info' => $th->getMessage()
            ], 200);
        }
    }
    public function AddKeluaran(Request $request)
    {

        $exist = DataKeluaran::where('tanggal', $request->tanggal)
            ->where('provider', $request->provider)
            ->first();
        if ($exist !== null) {
            return response([
                'success' => false,
                'msg' => "Data Tanggal $request->tanggal Sudah Pernah Diinput"
            ], 200);
        }

        try {
            DataKeluaran::updateOrCreate([
                'tanggal' => $request->tanggal,
                'provider' => $request->provider,

            ], [
                'periode' => $request->periode,
                'hari' => $request->hari,
                'keluaran' => $request->keluaran,

            ]);
            return response([
                'success' => true,
                'msg' => "Data Tanggal $request->tanggal Berhasil Diinput"
            ], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return response([
                'success' => false,
                'msg' => $th->getMessage()
            ], 200);
        }

        // try {
        //     $hari = $request->hari;
        //     $tanggal = $request->tanggal;
        //     $periode = $request->periode;
        //     $keluaran = (string) $this->base4($request->keluaran);
        //     $keluaran = (string) str_pad($keluaran, 4, '0', STR_PAD_LEFT);
        //     $last2 = substr($keluaran, 2);
        //     $hasilHitung =  AngkaDetect::hitungAngka($keluaran);


        //     $dataBaru =  new DataKeluaran();
        //     $dataBaru->hari = $hari;
        //     $dataBaru->tanggal = $tanggal;
        //     $dataBaru->periode = $periode;
        //     $dataBaru->provider = $request->provider;
        //     $dataBaru->keluaran = $keluaran;

        //     try {
        //         $dataBaru->save();
        //         $lastInsert =  $dataBaru->id;
        //         // $lastInsert = '1';
        //     } catch (\Throwable $th) {
        //         throw $th;
        //     }
        //     return  $this->_prosesKeluaran($dataBaru, 'create');

        //     return response([
        //         'success' => true,
        //         'msg' => "Data Tanggal $request->tanggal Berhasil Diinput"
        //     ], 200);
        // } catch (\Throwable $th) {
        //     return response([
        //         'success' => false,
        //         'msg' => $th->getMessage()
        //     ], 200);
        // }
    }

    // api usage end
    public function graph()
    {
        return  $data =  (object) $this->_getData(16);
        //  $data->_askor;
        return view('graph', ['data' => $data]);
    }

    public function index()
    {

        return view('home');

        return $this->_getData(16);
    }

    public function updateAll()
    {
        $allProvider  = DataProvider::all();
        foreach ($allProvider as $provider) {
            $lastKeluaran = DataKeluaran::where('provider', $provider->id)->orderBy('id', 'desc')->first();
            $iddata = $lastKeluaran->id;

            $hari = $lastKeluaran->hari;
            $tanggal = $lastKeluaran->tanggal;
            $periode = $lastKeluaran->periode;
            $keluaran = $lastKeluaran->keluaran;
            $keluaran = str_pad($keluaran, 4, '0', STR_PAD_LEFT);
            $last2 = substr($keluaran, 2);
            $hasilHitung =  AngkaDetect::hitungAngka($keluaran);


            $lastInsert = $lastKeluaran->id;


            // return $hasilHitung['_report'];
            // return $hasilHitung['_report'];
            $explodeColok =  explode(',', $hasilHitung['_report']['_colokBebas']);
            $arrayColok = [
                "id_keluaran" => $lastInsert,
                "angka_0" => $explodeColok[0],
                "angka_1" => $explodeColok[1],
                "angka_2" => $explodeColok[2],
                "angka_3" => $explodeColok[3],
                "angka_4" => $explodeColok[4],
                "angka_5" => $explodeColok[5],
                "angka_6" => $explodeColok[6],
                "angka_7" => $explodeColok[7],
                "angka_8" => $explodeColok[8],
                "angka_9" => $explodeColok[9],


            ];

            $arrayAkke = [
                'id_keluaran' => $lastInsert,
                'as' => $hasilHitung['_report']['_akke']['AS'],
                'kop' => $hasilHitung['_report']['_akke']['KOP'],
                'kepala' => $hasilHitung['_report']['_akke']['KEPALA'],
                'ekor' => $hasilHitung['_report']['_akke']['EKOR'],

            ];
            // insert shio
            $arrayShio = [
                "id_keluaran" => $lastInsert,
                "angka" => $last2,
                "shio" => $hasilHitung['_report']['_shio']
            ];


            DataShio::where('id_keluaran', $iddata)->delete();
            DataAKKE::where('id_keluaran', $iddata)->delete();
            DataUmum::where('id_keluaran', $iddata)->delete();
            DataColokBebas::where('id_keluaran', $iddata)->delete();


            DataShio::create($arrayShio);
            DataAKKE::create($arrayAkke);
            DataColokBebas::create($arrayColok);



            $array5050 = [];
            foreach ($hasilHitung['_report']['_5050']['umum_'] as $umum) {
                array_push($array5050, [
                    "id_keluaran" => $lastInsert,
                    "angka" => $last2,
                    "status" => $umum,
                    "jenis" => "UMUM"
                ]);
            }

            foreach ($hasilHitung['_report']['_5050']['kombinasi_'] as $kombinasi) {
                array_push($array5050, [
                    "id_keluaran" => $lastInsert,
                    "angka" => $last2,
                    "status" => $kombinasi,
                    "jenis" => "KOMBINASI"
                ]);
            }

            // hapus data umum lama

           
            foreach ($array5050 as $a5050) {
                DataUmum::create($a5050);
            }

            echo "Provider $lastKeluaran->provider updated <br/>";
        }
    }

    public function keluaranProviderByName($providerName)
    {
        // return $providerName;
        $provider =  DataProvider::where('nama', $providerName)->first();
        if ($provider == null) {
            $provider = DataProvider::where('kode', $providerName)->first();
        }

        // return  $this->_getData($provider->id);

        return response([
            'success' => true,
            'report' => $this->_getData($provider->id)
        ], 200);
        // if ($provider !== null) {
        //     return response([
        //         'success' => true,

        //         'report' => $this->_getData($provider->id)
        //     ], 200);
        // } else {
        //     return response([
        //         'success' => false,
        //         'msg' => 'Provider name is invalid'
        //     ], 200);
        // }
    }



    public function _insertData($data)
    {
    }

    private function namaHari($besok = false)
    {
        $hari = date("D");
        if ($besok == true) {
            // $hari = date("D",+1);
            $hari =  date("D", strtotime('tomorrow'));
        }
        // return $hari;

        switch ($hari) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;

            case 'Mon':
                $hari_ini = "Senin";
                break;

            case 'Tue':
                $hari_ini = "Selasa";
                break;

            case 'Wed':
                $hari_ini = "Rabu";
                break;

            case 'Thu':
                $hari_ini = "Kamis";
                break;

            case 'Fri':
                $hari_ini = "Jumat";
                break;

            case 'Sat':
                $hari_ini = "Sabtu";
                break;

            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }

        return $hari_ini;
    }
    private function detectHariEsok($tanggal)
    {
        $date1 = str_replace('-', '/', $tanggal);
        $hariLengkap = date('Y-m-d', strtotime($date1 . "+1 days"));
        $hari = date('D', strtotime($date1 . "+1 days"));
        // $hari =  date("D", strtotime($tanggal + 1));
        // return $hari;

        switch ($hari) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;

            case 'Mon':
                $hari_ini = "Senin";
                break;

            case 'Tue':
                $hari_ini = "Selasa";
                break;

            case 'Wed':
                $hari_ini = "Rabu";
                break;

            case 'Thu':
                $hari_ini = "Kamis";
                break;

            case 'Fri':
                $hari_ini = "Jumat";
                break;

            case 'Sat':
                $hari_ini = "Sabtu";
                break;

            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }
        return [
            'hari' => $hari_ini,
            'tanggal' => implode('-', array_reverse(explode('-', $hariLengkap)))
        ];
        // return $hari_ini;
    }

    public function _getData($id, $limit = 30)
    {
        $provider = $id;
        $last30DaysData =   DataKeluaran::select(
            'id',
            'hari',
            'tanggal',
            'periode',
            'keluaran',
            'provider'
        )->where('provider', $provider)
            ->orderBy('tanggal', 'DESC')
            ->limit($limit)
            ->get();
        $last30daysDataID =  $last30DaysData->pluck('id');




        // return $this->processAkke($last30daysDataID);

        // $lastData = Dat
        $hariini =  DataKeluaran::where('provider', $provider)->orderBy('tanggal', 'desc')->first();
        $tanggalHariIni  =  $hariini->tanggal;

        return  array(
            '_today' => DataKeluaran::where('provider', $provider)->orderBy('tanggal', 'desc')
                ->first(),
            '_tomorrow' => $this->detectHariEsok($tanggalHariIni),
            '_info' => DataProvider::find($provider),
            '_dasar' =>  $this->processDasar($last30daysDataID),
            '_pola2D' =>  $this->processPola2D($last30daysDataID),
            '_umum' => $this->process5050Umum($last30daysDataID),
            '_kombinasi' => $this->process5050Kombinasi($last30daysDataID),
            '_shioReport' => $this->processShio($last30daysDataID),
            '_colokBebasReport' => $this->processColokBebas($last30daysDataID),
            '_last30DaysReport' => $last30DaysData,
            '_askor' => $this->processAkke($last30daysDataID),

        );
    }

    private function processPola2D($id)
    {
        $data = DB::table('data_keluaran')
            ->whereIn('id', $id)
            ->orderBy('tanggal', 'DESC')
            ->limit(10)
            ->get();


        $res = [];
        foreach ($data as $key => $value) {
            //batasi 10
            if ($key < 10) {
                // do something here
                $hasil =  AngkaDetect::genapGanjil($value->keluaran);
                $res[$value->keluaran] = $hasil;


                // // array_push($sm,$value->keluaran);
                // $array_hasil = $hasil;
                // // // $array_hasil = explode(', ', $hasil);
                // foreach ($array_hasil as $val) {
                //     $res[$val] = $res[$val] + 1;
                // }
            }
        }
        return $res;
    }
    private function processDasar($id)
    {
        $data = DB::table('data_keluaran')
            ->whereIn('id', $id)
            ->get();
        $res = [
            'BESAR' => 0,
            'KECIL' => 0,
            'GANJIL' => 0,
            'GENAP' => 0,
        ];
        foreach ($data as $data) {
            $hasil = AngkaDetect::cariDasarAngkaMain($data->keluaran);
            $array_hasil = explode(', ', $hasil);
            foreach ($array_hasil as $val) {
                $res[$val] = $res[$val] + 1;
            }

            // array_push($res, AngkaDetect::cariDasarAngkaMain($data->keluaran));
        }
        return $res;
    }

    private function processAkke($id)
    {
        // DB::enableQueryLog(); // Enable query log

        $as  =  DB::table('data_akke')
            ->select(
                DB::raw('`as` as nomor, COUNT(*) AS `jlh` ')
            )->whereIn('id_keluaran', $id)
            ->groupBy('nomor')
            // ->orderBy(DB::raw('count(`as`)'), 'DESC')
            ->get();
        // dd(DB::getQueryLog());
        $kop  =  DB::table('data_akke')
            ->select(
                DB::raw('`kop` as nomor, COUNT(*) AS `jlh` ')
            )->whereIn('id_keluaran', $id)
            ->groupBy('nomor')
            // ->orderBy(DB::raw('count(`as`)'), 'DESC')
            ->get();

        $kepala  =  DB::table('data_akke')
            ->select(
                DB::raw('`kepala` as nomor, COUNT(*) AS `jlh` ')
            )->whereIn('id_keluaran', $id)
            ->groupBy('nomor')
            // ->orderBy(DB::raw('count(`as`)'), 'DESC')
            ->get();

        $ekor  =  DB::table('data_akke')
            ->select(
                DB::raw('`ekor` as nomor, COUNT(*) AS `jlh` ')
            )->whereIn('id_keluaran', $id)
            ->groupBy('nomor')
            // ->orderBy('ekor', 'DESC')
            ->get();


        // return $this->filterAskor($ekor);

        return [
            'as' => $this->filterAskor($as),
            'kop' => $this->filterAskor($kop),
            'kepala' => $this->filterAskor($kepala),
            'ekor' => $this->filterAskor($ekor),
        ];
    }

    private function filterAskor($array)
    {
        // return $array;
        $sementara = [];

        $sementara['data'] = [];
        $sementara['jumlahganjil'] = 0;
        $sementara['jumlahgenap'] = 0;
        $sementara['jumlahkiri'] = 0;
        $sementara['jumlahkanan'] = 0;
        $sementara['kiri'] = '';
        $sementara['kanan'] = '';
        $sementara['raw'] = $array;
        $statusKiriCounterKecil = 0;
        $statusKiriCounterBesar = 0;
        $statusKananCounterKecil = 0;
        $statusKananCounterBesar = 0;
        for ($i = 0; $i < 10; $i++) {

            $sementara['data'][$i] = 0;
        }
        $x = 0;
        $jumlahKiri = 0;
        $jumlahKanan = 0;
        $jumlahGenap = 0;
        $jumlahGanjil = 0;
        foreach ($array as $key => $kor) {
            if ($kor->nomor % 2 == 0) {
                $jumlahGenap  = $jumlahGenap + $kor->jlh;
            } else {
                $jumlahGanjil =  $jumlahGanjil + $kor->jlh;
            }
            if ($kor->nomor < 5) {
                $jumlahKiri  = $jumlahKiri + $kor->jlh;
            } else {
                $jumlahKanan  = $jumlahKanan + $kor->jlh;
            }
            $sementara['data'][$kor->nomor] = $kor->jlh;
        }
        $sementara['jumlahkiri'] = $jumlahKiri;
        $sementara['jumlahkanan'] = $jumlahKanan;
        // $jumlahGenap = 0;
        // $jumlahGanjil = 0;
        // for ($i = 0; $i < 10; $i++) {
        //     if ($i % 2 == 0) {
        //         $jumlahGenap++;
        //     } else {
        //         $jumlahGanjil++;
        //     }
        //     if ($i < 5) {
        //         ($sementara['data'][$i] < 5) ? $statusKiriCounterKecil++ : $statusKiriCounterBesar++;
        //     } else {
        //         ($sementara['data'][$i] < 5) ? $statusKananCounterKecil++ : $statusKananCounterBesar++;
        //     }
        // }
        // $sementara['kiri'] = ($statusKiriCounterKecil > $statusKiriCounterBesar) ? 'BESAR' : 'KECIL';
        // if ($jumlahGenap > $jumlahGanjil) {
        //     $sementara['kanan'] = 'GENAP';
        // } elseif ($jumlahGenap < $jumlahGanjil) {
        //     $sementara['kanan'] = 'GANJIL';
        // } else {
        //     $sementara['kanan'] = 'RATA';
        // }
        // $sementara['kanan'] = ($jumlahGenap > 5) ? 'GENAP' : ($jumlahGenap == "5")  ? 'RATA' : 'GANJIL';
        // cari ganjil genap
        if ($jumlahGanjil === $jumlahGenap) {
            $sementara['kanan'] = 'RATA';
        } elseif ($jumlahGanjil > $jumlahGenap) {
            $sementara['kanan'] = 'GANJIL';
        } else {
            $sementara['kanan'] = 'GENAP';
        }


        if ($jumlahKiri == $jumlahKanan) {
            $sementara['kiri'] = 'RATA';
        } else if ($jumlahKiri > $jumlahKanan) {
            $sementara['kiri'] = 'KECIL';
        } else {
            $sementara['kiri'] = 'BESAR';
        }
        // $sementara['kanan'] =($jumlahGanjil > $jumlahGenap) ? 'GANJIL' : ($jumlahGanjil < $jumlahGenap) ? 'GENAP' : 'RATA';
        // $sementara['kiri'] = ($jumlahKiri > $jumlahKanan) ? 'KECIL' : ($jumlahKiri < $jumlahKanan) ? 'BESAR' : 'RATA';
        $sementara['jumlahganjil'] = $jumlahGanjil;
        $sementara['jumlahgenap'] = $jumlahGenap;
        return $sementara;
    }

    // SELECT `as`, COUNT(*) AS jlh FROM data_akke GROUP BY `as`


    private function process5050Umum($id)
    {

        // dd($id);
        $data =  DB::table('data_5050')
            ->select('status', DB::raw('count(status) as jumlah'))
            ->where('jenis', 'UMUM')
            ->whereIn('id_keluaran', $id)
            ->groupBy('status')
            ->orderBy(DB::raw('count(status)'), 'DESC')
            ->get();
        return $data;
        // dd($data);
    }
    private function process5050Kombinasi($id)
    {

        return DB::table('data_5050')
            ->select('status', DB::raw('count(status) as jumlah'))
            ->where('jenis', 'KOMBINASI')
            ->whereIn('id_keluaran', $id)
            ->groupBy('status')
            ->orderBy(DB::raw('count(status)'), 'DESC')
            ->get();
    }

    private function processColokBebas($id)
    {

        // return $id[29];
        $data =  DB::table('data_colokbebas')
            ->select(DB::raw('SUM(angka_0) AS "Angka 0",
        SUM(angka_1) AS "Angka 1",
        SUM(angka_2) AS "Angka 2",
        SUM(angka_3) AS "Angka 3",
        SUM(angka_4) AS "Angka 4",
        SUM(angka_5) AS "Angka 5",
        SUM(angka_6) AS "Angka 6",
        SUM(angka_7) AS "Angka 7",
        SUM(angka_8) AS "Angka 8",
        SUM(angka_9) AS "Angka 9"'))
            ->whereIn('id_keluaran', $id)
            ->get();

        return [
            'data' => $data,
            'total' => array_sum((array) $data[0])
        ];

        // return array_sum((array)$data[0]);
    }
    private function processShio($id)
    {

        return DB::table('data_shio')
            ->select('shio', DB::raw('count(shio) as jlh'))
            ->whereIn('id_keluaran', $id)
            ->groupBy('shio')
            ->orderBy(DB::raw('count(shio)'), 'DESC')
            ->get();
    }


    public function insertDummy($id)
    {
        $angkaKeluar = 3134;
        // shio
        // $arr =  $this->arrayShio();
        // return $arr["01"];

        // akke

        // return AngkaDetect::hitungAngka($angkaKeluar);
        foreach ($this->dummyData30() as $dummy) {
            $hari = $dummy[0];
            $tanggal = $dummy[1];
            $periode = $dummy[2];
            $keluaran = $dummy[3];
            $keluaran = str_pad($keluaran, 4, '0', STR_PAD_LEFT);
            $last2 = substr($keluaran, 2);
            $hasilHitung =  AngkaDetect::hitungAngka($keluaran);


            $dataBaru =  new DataKeluaran();
            $dataBaru->hari = $hari;
            $dataBaru->tanggal = $tanggal;
            $dataBaru->periode = $periode;
            $dataBaru->provider = $id;
            $dataBaru->keluaran = $keluaran;

            try {
                $dataBaru->save();
                $lastInsert =  $dataBaru->id;
                // $lastInsert = '1';
            } catch (\Throwable $th) {
                throw $th;
            }

            // return $hasilHitung['_report'];
            // return $hasilHitung['_report'];
            $explodeColok =  explode(',', $hasilHitung['_report']['_colokBebas']);
            $arrayColok = [
                "id_keluaran" => $lastInsert,
                "angka_0" => $explodeColok[0],
                "angka_1" => $explodeColok[1],
                "angka_2" => $explodeColok[2],
                "angka_3" => $explodeColok[3],
                "angka_4" => $explodeColok[4],
                "angka_5" => $explodeColok[5],
                "angka_6" => $explodeColok[6],
                "angka_7" => $explodeColok[7],
                "angka_8" => $explodeColok[8],
                "angka_9" => $explodeColok[9],


            ];

            $arrayAkke = [
                'id_keluaran' => $lastInsert,
                'as' => $hasilHitung['_report']['_akke']['AS'],
                'kop' => $hasilHitung['_report']['_akke']['KOP'],
                'kepala' => $hasilHitung['_report']['_akke']['KEPALA'],
                'ekor' => $hasilHitung['_report']['_akke']['EKOR'],

            ];
            // insert shio
            $arrayShio = [
                "id_keluaran" => $lastInsert,
                "angka" => $last2,
                "shio" => $hasilHitung['_report']['_shio']
            ];
            DataShio::create($arrayShio);
            DataAKKE::create($arrayAkke);
            DataColokBebas::create($arrayColok);
            $array5050 = [];
            foreach ($hasilHitung['_report']['_5050']['umum_'] as $umum) {
                array_push($array5050, [
                    "id_keluaran" => $lastInsert,
                    "angka" => $last2,
                    "status" => $umum,
                    "jenis" => "UMUM"
                ]);
            }

            foreach ($hasilHitung['_report']['_5050']['kombinasi_'] as $kombinasi) {
                array_push($array5050, [
                    "id_keluaran" => $lastInsert,
                    "angka" => $last2,
                    "status" => $kombinasi,
                    "jenis" => "KOMBINASI"
                ]);
            }

            foreach ($array5050 as $a5050) {
                DataUmum::create($a5050);
            }











            // mulai masukkan data
            // die;
        }
    }

    private function dummyData30()
    {
        return array(
            ["Sabtu", " 2020-03-07", "HK - 1712", "2182"],
            ["Jumat", " 2020-03-06", "HK - 1711", "7478"],
            ["Kamis", " 2020-03-05", "HK - 1710", "1700"],
            ["Selasa", " 2020-03-03", "HK - 1708", "4365"],
            ["Senin", " 2020-03-02", "HK - 1707", "3399"],
            ["Minggu", " 2020-03-01", "HK - 1706", "4725"],
            ["Sabtu", " 2020-02-29", "HK - 1705", "7893"],
            ["Jumat", " 2020-02-28", "HK - 1704", "5679"],
            ["Kamis", " 2020-02-27", "HK - 1703", "3754"],
            ["Rabu", " 2020-02-26", "HK - 1702", "6625"],
            ["Selasa", " 2020-02-25", "HK - 1701", "7013"],
            ["Senin", " 2020-02-24", "HK - 1700", "3765"],
            ["Minggu", " 2020-02-23", "HK - 1699", "6073"],
            ["Sabtu", " 2020-02-22", "HK - 1698", "286"],
            ["Jumat", " 2020-02-21", "HK - 1697", "3741"],
            ["Kamis", " 2020-02-20", "HK - 1696", "3432"],
            ["Rabu", " 2020-02-19", "HK - 1695", "7289"],
            ["Selasa", " 2020-02-18", "HK - 1694", "3014"],
            ["Senin", " 2020-02-17", "HK - 1693", "256"],
            ["Minggu", " 2020-02-16", "HK - 1692", "8291"],
            ["Sabtu", " 2020-02-15", "HK - 1691", "4185"],
            ["Jumat", " 2020-02-14", "HK - 1690", "9180"],
            ["Kamis", " 2020-02-13", "HK - 1689", "7120"],
            ["Rabu", " 2020-02-12", "HK - 1688", "8084"],
            ["Selasa", " 2020-02-11", "HK - 1687", "5908"],
            ["Senin", " 2020-02-10", "HK - 1686", "9640"],
            ["Minggu", " 2020-02-09", "HK - 1685", "613"],
            ["Sabtu", " 2020-02-08", "HK - 1684", "7320"],
            ["Jumat", " 2020-02-07", "HK - 1683", "6820"],
            ["Kamis", " 2020-02-06", "HK - 1682", "8641"]


        );
    }
}
