<?php

namespace App\Http\Controllers;

use App\Model\DataUmum;
use Illuminate\Http\Request;

class DataUmumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\DataUmum  $dataUmum
     * @return \Illuminate\Http\Response
     */
    public function show(DataUmum $dataUmum)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\DataUmum  $dataUmum
     * @return \Illuminate\Http\Response
     */
    public function edit(DataUmum $dataUmum)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\DataUmum  $dataUmum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataUmum $dataUmum)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\DataUmum  $dataUmum
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataUmum $dataUmum)
    {
        //
    }
}
