<?php

namespace App\Http\Controllers;

use App\Model\DataSpesial;
use Illuminate\Http\Request;

class DataSpesialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\DataSpesial  $dataSpesial
     * @return \Illuminate\Http\Response
     */
    public function show(DataSpesial $dataSpesial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\DataSpesial  $dataSpesial
     * @return \Illuminate\Http\Response
     */
    public function edit(DataSpesial $dataSpesial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\DataSpesial  $dataSpesial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataSpesial $dataSpesial)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\DataSpesial  $dataSpesial
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataSpesial $dataSpesial)
    {
        //
    }
}
