<?php

namespace App\Http\Controllers;

use App\Model\DataKeluaran;
use App\Model\DataProvider;
use DiDom\Document as DidomDoc;
use DiDom\query as DidomQueery;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
// use Didom\Document;
// use Didom\query;

use Illuminate\Http\Request;

class TogelGrabberController extends Controller
{
    public function __construct()
    {
        // return $this->detectGrab($url);
    }

    public static function startGrabbing($url)
    {


        return TogelGrabberController::detectGrab($url);
    }

    public function doGrab($param)
    {

        if ($param === 'all') {
            $data =  $this->getProviderUrl('all');
            foreach ($data as $key => $val) {
                $this->detectGrab($key);
            }
        } else {
            $this->detectGrab($param);
        }
    }


    public function detectGrab($providerName)
    {
        // return $providerName;

        $idProvider =  DataProvider::where('kode', strtoupper($providerName))->first()->id;
        $url = trim($this->getProviderUrl($providerName));
        if (strpos($url, '35.240.254.175') !== false) {
            // return TogelGrabber::startGrabbing($url);
            $request =  $this->togelKita($url);
        } else if (strpos($url, '.csv') !== false) {
            // return TogelGrabber::startGrabbing($url);
            // return $providerName;


            $request = $this->parsingCsvRajaHasil($url);
        } else if (strpos($url, 'bandot4d') !== false) {
            // return TogelGrabber::startGrabbing($url);
            return $this->parsingBandot4d($url);
        } else if (strpos($url, '165.22.253.162') !== false) {
            // return TogelGrabber::startGrabbing($url);
            $request =  $this->parsingRajaHasil($url);
        }

        $this->insertData($request, $idProvider);
        echo $providerName ." Success \n";
    }

    private function insertData($request, $idProvider)
    {
        foreach ($request as $request) {
            DataKeluaran::updateOrCreate([
                'tanggal' => $request['tanggal'],
                'provider' => $idProvider,

            ], [
                'periode' => $request['periode'],
                'hari' => $request['hari'],
                'keluaran' => $request['keluaran'],

            ]);
            // return $data['tanggal'];
        }
    }

    private function curlRajaHasil($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Cache-Control: max-age=0';
        $headers[] = 'Upgrade-Insecure-Requests: 1';
        $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36';
        $headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9';
        $headers[] = 'Referer: http://165.22.253.162/';
        $headers[] = 'Accept-Language: en-US,en;q=0.9';
        // $headers[] = 'Cookie: _ga=GA1.1.1412126398.1583589787; _hjid=9c0ac85f-b6eb-4bb4-a460-80d65a6908dc; _gid=GA1.1.808786277.1585205713';
        $headers[] = 'If-None-Match: W/\"918f-tueIXGEcYVnXWBR93ZJpzqJ10sE\"';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }
    private function parsingRajaHasil($url)
    {
        $result = [];
        $data =  $this->togelKitaCurl($url);
        // $data =  $this->curlRajaHasil($url);
        $document = new DidomDoc($data);
        // return $document->find('div.latest')[0]->html();
        if ($document->has('div.row')) {

            $x = 0;

            foreach ($document->find('div.row') as $tr) {

                if ($x >= 1) {

                    $td = $tr->find('div');
                    // return $td[1]->text();
                    $pecah = explode(', ', $td[1]->text());
                    $date = $pecah[1];
                    $tanggal = explode('-', $date);

                    array_push($result, [
                        'tanggal' => $tanggal[2] . '-' . $tanggal[1] . '-' . $tanggal[0],
                        'hari' => trim($pecah[0]),
                        'periode' => explode('-', $td[2]->text())[1],
                        'keluaran' => $td[3]->text(),
                    ]);
                }
                $x++;
            }
            return $result;
        }
    }

    private function parsingBandot4d($url)
    {
        $result = [];
        for ($i = 1; $i < 6; $i++) {
            $data =  $this->togelKitaCurl($url . $i);
            $document = new DidomDoc($data);
            $table = $document->find('table')[0];
            $x = 0;
            foreach ($table->find('tr') as $tr) {
                if ($x >= 1) {

                    $td = $tr->find('td');
                    $pecah = explode(' ', $td[1]->text());
                    $date = $pecah[1];
                    $bulan =  str_pad(date("n", strtotime($date)), 2, '0', STR_PAD_LEFT);


                    array_push($result, [
                        'tanggal' => $pecah[0] . '-' . $bulan . '-' . $pecah[2],
                        'hari' => $td[2]->text(),
                        'periode' => explode(' - ', $td[3]->text())[1],
                        'keluaran' => $td[4]->text(),
                    ]);
                }
                $x++;
            }
        }
        return $result;
    }

    private function curlCsv($url)
    {

        return $this->togelKitaCurl($url);
    }

    private function parsingCsvRajaHasil($url)
    {
        $result = [];
        // $data = $this->curlCsv($url);
        $data = $this->curlCsvx($url);
        // return  $data = file_get_contents($url);
        $rows = explode("\n", $data);
        $s = array();
        $x = 0;
        foreach ($rows as $row) {
            if ($x >= 1) {

                $td =   str_getcsv($row);
                $pecah1 = explode(', ', $td[0]);
                $pecah2 = explode('-', $pecah1[1]);
                array_push($result, [
                    'tanggal' => $pecah2[2] . '-' . $pecah2[1] . '-' . $pecah2[0],
                    'hari' => $pecah1[0],
                    'periode' => explode('-', $td[1])[1],
                    'keluaran' => $td[2],
                ]);
            }
            $x++;
        }

        return $result;
    }


    private function togelKita($url)
    {
        $result = [];
        $data =  $this->togelKitaCurl($url);
        $document = new DidomDoc($data);
        $table =  $document->find('table#keluaran_full')[0];
        $x = 0;
        foreach ($table->find('tr') as $tr) {
            if ($x > 1) {

                $td = $tr->find('td');
                array_push($result, [
                    'tanggal' => $td[1]->text(),
                    'hari' => $td[2]->text(),
                    'periode' => explode('-', $td[3]->text())[1],
                    'keluaran' => $td[4]->text(),
                ]);
                //    foreach($tr->find('td') as $td){
                //    return $td->html();
                //
                //    }
            }
            $x++;
        }
        return $result;
        // return $table = $document->find('table#datatable')[0];
    }
    private function curlCsvx($csvUrl)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $csvUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


        $headers = array();
        $headers[] = 'Accept-Charset: utf-8';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }

    private function togelKitaCurl($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        // curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Cache-Control: max-age=0';
        $headers[] = 'Upgrade-Insecure-Requests: 1';
        $headers[] = 'User-Agent: Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.114 Mobile Safari/537.36';
        $headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9';
        $headers[] = 'Accept-Language: en-US,en;q=0.9';
        // $headers[] = 'Cookie: _ga=GA1.1.1066444564.1584716234; HstCfa4078542=1584716234416; HstCmu4078542=1584716234416; __dtsu=4C3015806174048D38D264957BB0E471; ci_session=v2a47dfk6def8531el2hgrl9oq97s31k; _gid=GA1.1.2075188913.1585246016; HstCnv4078542=2; HstCla4078542=1585249529321; HstPn4078542=3; HstPt4078542=10; HstCns4078542=5';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }

    private function guzzleGrab($url)
    {
        $client = new Client();
        $response =  $client->get($url);

        if ($response->getStatusCode() == 200) {
            return [
                'success' => true,
                'html' => $response->getBody()
            ];
            // return $response->getBody();
        }
    }



    public function getProviderUrl($name)
    {
        // ["Singapore", "SGP", "https://1.bp.blogspot.com/-lxlQ2hfuSqk/XnyrrmUwwvI/AAAAAAAAJtM/5XkjzFEPVg0zkOl9_RAOPr0TywtZmRNJACLcBGAsYHQ/s1600/Singapore.jpg", null, null, "1"],
        // ["Sydney", "SD", "https://1.bp.blogspot.com/-W9-Q-NUci_Q/Xnyrty_hEnI/AAAAAAAAJtk/MnvVfhyZ6z8w2JKKdeUN2sCRt_i_TR6NQCLcBGAsYHQ/s1600/sydney.jpg", null, null, "2"],
        // ["Hongkong", "HK", "https://1.bp.blogspot.com/-KdeykL7SYbo/Xnyq-2bCCmI/AAAAAAAAJsQ/iJeRWjXUQzUzJcl0aD0NrYarLSQNx6gOwCLcBGAsYHQ/s1600/Hongkong.jpg", null, null, "3"],
        // ["Timor", "TMR", "https://1.bp.blogspot.com/-Uea5FB1nOrk/XnyrtSezryI/AAAAAAAAJtc/C9Yfl_i-De4cW-SF_eIRFUUD4uCP8EhWQCLcBGAsYHQ/s1600/Timor.jpg", null, null, "4"],
        // ["Bullseye", "BUL", "https://1.bp.blogspot.com/-858d1umKzWA/Xnyq96TRXoI/AAAAAAAAJsI/bMq43CfP8L8LW5djZD1B5eiA4KjjdmcCgCLcBGAsYHQ/s1600/Bullseye.jpg", null, null, "5"],

        // ["Seoul", "SOL", "https://1.bp.blogspot.com/-_a6mEMtEQEQ/XnyrsOWWofI/AAAAAAAAJtQ/ns47Nd2Ts4okm_LTaN1eUyArUIY4fuI3wCLcBGAsYHQ/s1600/Seoul.jpg", null, null, "8"],
        // ["China", "CHN", "https://1.bp.blogspot.com/-kJXR1tjrfPQ/Xnyq-PktHWI/AAAAAAAAJsM/ouKlQ8gRvOc8OHm5ws7yfd1z3LFmEJ5uwCLcBGAsYHQ/s1600/China.jpg", null, null, "9"],
        // ["Bangkok", "BK", "https://1.bp.blogspot.com/-7-PL7n9lb6Q/Xnyq7hFQFJI/AAAAAAAAJr4/oy3s66gB0dEJNiFRTDl1Yk8PpTaH_xsZwCLcBGAsYHQ/s1600/Bangkok.jpg", null, null, "10"],
        // ["Singapore45", "SGP45", "https://1.bp.blogspot.com/-esG2iZOLXBw/XnyrsYRURWI/AAAAAAAAJtU/UETbAoqecEAOq4zobMksEF1U7fX9RD9UACLcBGAsYHQ/s1600/Singapore45.jpg", null, null, "11"],
        // ["Milan", "MIL", "https://1.bp.blogspot.com/-ckAmGFPvkMw/XnyrpsBOOzI/AAAAAAAAJtA/p_bPdfKzxVQyVEPpXVfY4x65ZMQVLN5nACLcBGAsYHQ/s1600/Milan.jpg", null, null, "12"],
        // ["Magnum4D", "MGN", "https://1.bp.blogspot.com/-RtX2wXCBzYM/XnyrAF5W8cI/AAAAAAAAJsc/QlzJ_osDkXg8sP6bDhcKC9z4zqPAZnQaACLcBGAsYHQ/s1600/Magnum4d.jpg", null, null, "13"],
        // ["Malaysia", "MY", "https://1.bp.blogspot.com/-AxhHs3ItMds/XnyrAtXc78I/AAAAAAAAJsg/kaBmoXvwtWAt2ayFvoYXCGBBN3cX7bZMACLcBGAsYHQ/s1600/Malaysia.jpg", null, null, "14"],
        // ["Japan", "JP", "https://1.bp.blogspot.com/-auFePNaRzMQ/Xnyq_vGpvnI/AAAAAAAAJsU/WjPFy8Kv_pQgx7ndKMyJ0l-Yok3gLWP2ACLcBGAsYHQ/s1600/Japan.jpg", null, null, "15"],
        // ["Manila", "MNL", "https://1.bp.blogspot.com/-RoYp6rIAT9Q/Xnyropdx-2I/AAAAAAAAJs8/RKjUUloZi4U0Wy2jbeRQtP9yWYZ3juo8QCLcBGAsYHQ/s1600/Manila.jpg", null, null, "16"],
        // ["Macau", "MC", "https://1.bp.blogspot.com/-42lgcwkQbTo/Xnyq_5j1JrI/AAAAAAAAJsY/uCnn2P_GC6AQ43Tie7shd2i_2H_yV5QQACLcBGAsYHQ/s1600/Macau.jpg", null, null, "17"],
        // ["PCSO", "PCSO", "https://1.bp.blogspot.com/-gmIo0Tk5klI/XnyrpruvzRI/AAAAAAAAJtE/FO0zR0uIFCAc8HrB0tYtMZfiCZlP1mzgQCLcBGAsYHQ/s1600/PCSO.jpg", null, null, "18"],
        // ["Taiwan", "TW", "https://1.bp.blogspot.com/-9PD3wh83S2Q/Xnyrs98Tl_I/AAAAAAAAJtY/GelxX_m2KgE8ZMQ8cZwpEwQgWzc1_4UkACLcBGAsYHQ/s1600/Taiwan.jpg", null, null, "19"],
        // ["Qatar", "QTR", "https://1.bp.blogspot.com/-R84y6tO4cUk/XnyrqRDoXgI/AAAAAAAAJtI/Sbq66NJUPKwJVqTLHuuDvH3ZFQBXJmwfACLcBGAsYHQ/s1600/Qatar.jpg", null, null, "20"],
        // ["Barcelona", "BCL", "https://1.bp.blogspot.com/-EbBamYy4KHs/Xnyq8Xug2SI/AAAAAAAAJsA/EYOSTtE-Sp4LAY83n98tWSRMwQib44cugCLcBGAsYHQ/s1600/Barcelona.jpg", null, null, "21"],
        // ["Berlin", "BER", "https://1.bp.blogspot.com/-K7Tabujfj5k/Xnyq9XOOpnI/AAAAAAAAJsE/5ekPcH8CtJogI_JgYio3GYWxJ7ximn1OwCLcBGAsYHQ/s1600/Berlin.jpg", null, null, "22"],


        $array = array(
            "BEI" => "http://35.240.254.175/results/pasaran/beijing",
            "TKY" => "http://35.240.254.175/results/pasaran/tokyo",
            "SOL" => "http://35.240.254.175/results/pasaran/seoul",
            "MIL" => "http://35.240.254.175/results/pasaran/milan",
            "MNL" => "http://35.240.254.175/results/pasaran/manila",
            "BCL" => "http://35.240.254.175/results/pasaran/barcelona",
            "HK" => "http://35.240.254.175/results/pasaran/hongkong",
            "TMR" => "http://35.240.254.175/results/pasaran/timor",
            "BER" => "http://35.240.254.175/results/pasaran/berlin",
            "BK" => "http://35.240.254.175/results/pasaran/bangkok",

            "PCSO" => "http://bandot4d.net/?content=hasil&pid=PCSO",
            "CHN" => "http://bandot4d.net/?content=hasil&pid=CHN",
            "MY" => "http://bandot4d.net/?content=hasil&pid=MY",
            "MC" => "http://bandot4d.net/?content=hasil&pid=MC",
            "SD" => "http://bandot4d.net/webdata.php?content=hasil&pid=SY&page=",
            "QTR" => "http://bandot4d.net/?content=hasil&pid=QTR",

            // "MGN" => "https://www.dementoto.fun/result/MAG",
            // "BUL" => "https://www.dementoto.fun/result/BUL",

            "TW" => "http://165.22.253.162/keluaran-togel-taiwan",
            "JP" => "http://165.22.253.162/keluaran-togel-japan",

            "SGP" => "http://35.240.254.175/results/pasaran/singapore",
            "SGP45" => "http://35.240.254.175/results/pasaran/singapore45",
        );

        if ($name == 'all') {
            return $array;
        }
        $name = strtoupper($name);

        return $array[$name];
    }
}
