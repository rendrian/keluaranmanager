<?php

namespace App\Http\Controllers;

use App\Model\DataKombinasi;
use Illuminate\Http\Request;

class DataKombinasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\DataKombinasi  $dataKombinasi
     * @return \Illuminate\Http\Response
     */
    public function show(DataKombinasi $dataKombinasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\DataKombinasi  $dataKombinasi
     * @return \Illuminate\Http\Response
     */
    public function edit(DataKombinasi $dataKombinasi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\DataKombinasi  $dataKombinasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataKombinasi $dataKombinasi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\DataKombinasi  $dataKombinasi
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataKombinasi $dataKombinasi)
    {
        //
    }
}
