<?php

namespace App\Providers;

use App\Http\Controllers\TogelController;
use App\Model\DataAKKE;
use App\Model\DataColokBebas;
use App\Model\DataShio;
use App\Model\DataUmum;
use Helperx\Facades\AngkaDetect;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');
        date_default_timezone_set('Asia/Jakarta');

        \App\Model\DataKeluaran::created(function ($keluaran) {
            // $hasil =  AngkaDetect::hitungAngka($keluaran->keluaran);

            // $x = new TogelController();
            // return    $x->_prosesKeluaran($hasil, 'create');
            // if ($keluaran->provider === "3") {

            // }

            $tanggalSekarang = Carbon::now()->toDateString();


            $angkakeluar = $keluaran->keluaran;
            // hitung hasil jika ada tambah data
            if ($keluaran->provider === 3) {
                Log::info('Hitung hasil mafiahasil');
                @file_get_contents("https://mafiahasil.top/api/hitunghasil");
                @file_get_contents("https://mafiahasil.top/api/hitungdukun/hongkong/$tanggalSekarang/$angkakeluar");
            } elseif ($keluaran->provider === 2) {
                Log::info('Hitung dukun togel sydney');
                Log::info('Exec  ' . "https://mafiahasil.top/api/hitungdukun/sydney/$tanggalSekarang/$angkakeluar");
                @file_get_contents("https://mafiahasil.top/api/hitungdukun/sydney/$tanggalSekarang/$angkakeluar");
            }


            $this->prosesKeluaranMasuk($keluaran, 'create');
        });

        \App\Model\DataKeluaran::deleted(function ($keluaran) {
            $this->prosessAfterDelete($keluaran);
        });

        \App\Model\DataKeluaran::updated(function ($keluaran) {
            $tanggalSekarang = Carbon::now()->toDateString();

            $angkakeluar = $keluaran->keluaran;

            // hitung hasil jika ada update data
            // dd("https://mafiahasil.top/api/hitungdukun/sydney/$tanggalSekarang/$angkakeluar");
            if ($keluaran->provider === 3) {
                Log::info('update Hitung hasil mafiahasil');
                file_get_contents("http://mafiahasil.vip/api/hitunghasil");
                file_get_contents("http://mafiahasil.vip/api/hitungdukun/hongkong/$tanggalSekarang/$angkakeluar");
                file_get_contents("http://mafiahasil.vip/api/hitungdukun/hongkong/$tanggalSekarang/$angkakeluar");
            } elseif ($keluaran->provider === 2) {
                Log::info('update Hitung dukun togel sydney');
                file_get_contents("http://mafiahasil.vip/api/hitungdukun/sydney/$tanggalSekarang/$angkakeluar");
                file_get_contents("http://mafiahasil.vip/api/hitungdukun/sydney/$tanggalSekarang/$angkakeluar");
            }
            $this->prosessAfterUpdate($keluaran);
        });
    }
    private function prosessAfterUpdate($keluaran)
    {
        $this->prosesKeluaranMasuk($keluaran);
    }
    private function prosessAfterDelete($keluaran)
    {
        DataShio::where('id_keluaran', $keluaran->id)->delete();
        DataAKKE::where('id_keluaran', $keluaran->id)->delete();
        DataUmum::where('id_keluaran', $keluaran->id)->delete();
        DataColokBebas::where('id_keluaran', $keluaran->id)->delete();
    }

    private function prosesKeluaranMasuk($request, $type = 'update')
    {
        try {
            if ($type == 'update') {
                $iddata = $request->id;
            } else {

                $iddata = $request->id;
            }

            $hari = $request->hari;
            $tanggal = $request->tanggal;
            $periode = null;
            $keluaran = $request->keluaran;
            $keluaran = str_pad($keluaran, 4, '0', STR_PAD_LEFT);
            $last2 = substr($keluaran, 2);
            $hasilHitung =  AngkaDetect::hitungAngka($keluaran);


            $lastInsert = $request->id;


            // return $hasilHitung['_report'];
            // return $hasilHitung['_report'];
            $explodeColok =  explode(',', $hasilHitung['_report']['_colokBebas']);
            $arrayColok = [
                "id_keluaran" => $lastInsert,
                "angka_0" => $explodeColok[0],
                "angka_1" => $explodeColok[1],
                "angka_2" => $explodeColok[2],
                "angka_3" => $explodeColok[3],
                "angka_4" => $explodeColok[4],
                "angka_5" => $explodeColok[5],
                "angka_6" => $explodeColok[6],
                "angka_7" => $explodeColok[7],
                "angka_8" => $explodeColok[8],
                "angka_9" => $explodeColok[9],


            ];

            $arrayAkke = [
                'id_keluaran' => $lastInsert,
                'as' => $hasilHitung['_report']['_akke']['AS'],
                'kop' => $hasilHitung['_report']['_akke']['KOP'],
                'kepala' => $hasilHitung['_report']['_akke']['KEPALA'],
                'ekor' => $hasilHitung['_report']['_akke']['EKOR'],

            ];
            // insert shio
            $arrayShio = [
                "id_keluaran" => $lastInsert,
                "angka" => $last2,
                "shio" => $hasilHitung['_report']['_shio']
            ];


            DataShio::where('id_keluaran', $iddata)->delete();
            DataAKKE::where('id_keluaran', $iddata)->delete();
            DataUmum::where('id_keluaran', $iddata)->delete();
            DataColokBebas::where('id_keluaran', $iddata)->delete();


            DataShio::create($arrayShio);
            DataAKKE::create($arrayAkke);
            DataColokBebas::create($arrayColok);



            $array5050 = [];
            foreach ($hasilHitung['_report']['_5050']['umum_'] as $umum) {
                array_push($array5050, [
                    "id_keluaran" => $lastInsert,
                    "angka" => $last2,
                    "status" => $umum,
                    "jenis" => "UMUM"
                ]);
            }

            foreach ($hasilHitung['_report']['_5050']['kombinasi_'] as $kombinasi) {
                array_push($array5050, [
                    "id_keluaran" => $lastInsert,
                    "angka" => $last2,
                    "status" => $kombinasi,
                    "jenis" => "KOMBINASI"
                ]);
            }

            // hapus data umum lama

            foreach ($array5050 as $a5050) {
                DataUmum::create($a5050);
            }

            return [
                'success' => true,
                'msg' => "Data di update"
            ];
        } catch (\Throwable $th) {

            return [
                'success' => false,
                'msg' => $th->getMessage()
            ];
        }
    }
}
