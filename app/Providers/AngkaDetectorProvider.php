<?php

namespace App\Providers;

use Helperx\AngkaDetect;
use Illuminate\Support\ServiceProvider;

class AngkaDetectorProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('AngkaDetect',function(){
            return new AngkaDetect;
        });
    }
}
