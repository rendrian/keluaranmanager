<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataKeluaran extends Model
{

    protected $table = 'data_keluaran';
    protected $fillable = [
        'hari',
        'tanggal',
        'periode',
        'keluaran',
        'provider',
    ];
    protected $hidden = [
        'created_at', 'updated_at',
    ];


    public function provider()
    {
        return $this->hasMany('App\Model\Provider');
    }


    protected $appends = [
        'idTanggal'
    ];
    public function getIdTanggalAttribute()
    {
        // $this->attributes['tanggal'] = 'lalalla';
        // return $value;
        return  date('d-m-Y', strtotime($this->attributes['tanggal']));
    }
}
