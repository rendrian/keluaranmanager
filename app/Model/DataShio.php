<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataShio extends Model
{
    protected $table = 'data_shio';
    protected $fillable = [
        'id_keluaran',
        'angka',
        'shio'

    ];
}
