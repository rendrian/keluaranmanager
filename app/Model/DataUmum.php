<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataUmum extends Model
{
    protected $table = 'data_5050';
    protected $fillable = [
        'id_keluaran',
        'angka',
        'jenis',
        'status',
    ];
}
