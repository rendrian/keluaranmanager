<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataColokBebas extends Model
{
    protected $table = 'data_colokbebas';
    protected $fillable = [
        'id_keluaran',
        'angka_0',
        'angka_1',
        'angka_2',
        'angka_3',
        'angka_4',
        'angka_5',
        'angka_6',
        'angka_7',
        'angka_8',
        'angka_9',
        
    ];
}
