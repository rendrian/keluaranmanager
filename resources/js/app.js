import 'bootstrap'
import Vue from 'vue';
import VueRouter from 'vue-router'
import { BootstrapVue, IconsPlugin, BootstrapVueIcons } from 'bootstrap-vue'
import ElementUI from 'element-ui'
import { ElementTiptapPlugin } from 'element-tiptap'
// import VueDraggable from 'vue-draggable'








// import svgSpriteLoader from './components/tiptap/helpers/svg-sprite-loader'


// const __svg__ = { path: './components/tiptap/assets/images/icons/*.svg', name: 'components/tiptap/assets/images/[hash].sprite.svg' }
// svgSpriteLoader(__svg__.filename)



// komponen
import App from './components/App'
import Index from './components/Index'
import Appnavbar from './components/Appnavbar'
import ListKeluaran from './components/Keluaran.list'
import Editkeluaran from './components/Keluaran.edit'
import Tambahkeluaran from './components/Keluaran.add'
import KeluaranByProvider from './components/Keluaran.byProvider'

import ListProvider from './components/Provider.list'
import AddProvider from './components/provider.add'
import EditProvider from './components/Provider.edit'





Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(BootstrapVueIcons)
Vue.use(ElementUI)
Vue.use(ElementTiptapPlugin)
// Vue.use(VueDraggable)




const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/home',
            name: 'home',
            component: Index
        },
        {
            path: '/keluaran-by-provider/:id',
            name: 'keluaranByProvider',
            component: KeluaranByProvider
        },
        {
            path: '/keluaran-add/:id/:kode',
            name: 'keluaranAdd',
            component: Tambahkeluaran
        },
        {
            path: '/keluaran-edit/:id',
            name: 'keluaranEdit',
            component: Editkeluaran
        },
        {
            path: '/keluaran-list',
            name: 'keluaranList',
            component: ListKeluaran
        },
        {
            path: '/provider-list',
            name: 'providerList',
            component: ListProvider
        },
        {
            path: '/provider-add',
            name: 'providerAdd',
            component: AddProvider
        },
        {
            path: '/provider-edit/:id',
            name: 'providerEdit',
            component: EditProvider
        }
    ]
});


const app = new Vue({
    el: '#app',
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-exact-active',
    components: { App, Appnavbar },
    router
});
