<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('dummy/{id}', 'TogelController@insertDummy');
// Route::get('/graph','TogelController@graph');
Route::get('adduser', function () {
  try {
    $new = new App\User;
    $new->name = "BGIBOLA";
    $new->email = "betgratisinfo@gmail.com";
    $new->password = Hash::make("aa12345678");
    $new->save();
  } catch (\Throwable $th) {
    return $th->getMessage();
  }

  return $new;
});

Auth::routes();
Route::get('/{any}', 'HomeController@index')->where('any', '.*');
Route::get('/', 'TogelController@index');

Route::get('/home', 'HomeController@index')->name('home');

