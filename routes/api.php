<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Model\DataKeluaran;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::get('wkwk', function () {
//     // $keluaran = new \App\Model\DataKeluaran;
//     // $keluaran->

//     // return DataKeluaran::find(1)->delete();

//     DataKeluaran::updateOrCreate([
//         'tanggal' => '2020-03-27',
//         'provider' => '10',

//     ], [
//         'hari' => 'Kamis',
//         'periode' => '1908',
//         'keluaran' => '8888',

//     ]);
// });


Route::group(['prefix' => 'data'], function () {
    Route::get('all.json', 'PublicController@getAllData');
});



Route::group(['prefix' => 'keluaran'], function () {
    Route::get('updateall', 'TogelController@updateAll');


    Route::get('byProvider/{id}', 'TogelController@getLast30DayData');
    Route::get('byProviderName/{name}', 'TogelController@keluaranProviderByName');
    Route::get('lastPeriode/{id}', 'TogelController@LastPeriode');
    Route::get('details/{id}', 'TogelController@keluaranDetails');
    Route::post('add', 'TogelController@AddKeluaran');
    Route::post('update', 'TogelController@UpdateKeluaran');
    Route::delete('delete', 'TogelController@DeleteKeluaran');
});

Route::group(['prefix' => 'provider'], function () {

    Route::get('prediksi/{providername}-{hari}-{tanggal}', 'ProviderController@prediksi');
    Route::get('list', 'ProviderController@index');
    Route::get('details/{id}', 'ProviderController@Details');
    Route::post('add', 'ProviderController@Add');
    Route::put('update', 'ProviderController@Update');
    Route::post('delete', 'ProviderController@Delete');
    Route::post('order', 'ProviderController@changeOrder');
});
